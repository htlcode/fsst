-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: mysql:3306
-- Generation Time: Sep 26, 2022 at 12:47 PM
-- Server version: 8.0.27
-- PHP Version: 7.4.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `Shop`
--

-- --------------------------------------------------------

--
-- Table structure for table `artikel`
--

CREATE TABLE `artikel` (
  `artnr` int NOT NULL,
  `artikel` varchar(40) NOT NULL,
  `preis` int NOT NULL,
  `whrg` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `kunden`
--

CREATE TABLE `kunden` (
  `knr` int NOT NULL,
  `nname` varchar(40) NOT NULL,
  `vname` varchar(40) NOT NULL,
  `str` varchar(40) NOT NULL,
  `nr` varchar(40) NOT NULL,
  `plz` int NOT NULL,
  `ort` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `rechnungen`
--

CREATE TABLE `rechnungen` (
  `rnr` int NOT NULL,
  `datum` date NOT NULL,
  `knr` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `rechnungspositionen`
--

CREATE TABLE `rechnungspositionen` (
  `rnr` int NOT NULL,
  `rpnr` int NOT NULL,
  `artnr` int NOT NULL,
  `anz` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `artikel`
--
ALTER TABLE `artikel`
  ADD PRIMARY KEY (`artnr`);

--
-- Indexes for table `kunden`
--
ALTER TABLE `kunden`
  ADD PRIMARY KEY (`knr`);

--
-- Indexes for table `rechnungen`
--
ALTER TABLE `rechnungen`
  ADD PRIMARY KEY (`rnr`),
  ADD KEY `fk_rechnungen_kunden` (`knr`);

--
-- Indexes for table `rechnungspositionen`
--
ALTER TABLE `rechnungspositionen`
  ADD PRIMARY KEY (`rnr`,`rpnr`),
  ADD KEY `fk_rechnungspositionen_artikel` (`artnr`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `rechnungen`
--
ALTER TABLE `rechnungen`
  ADD CONSTRAINT `fk_rechnungen_kunden` FOREIGN KEY (`knr`) REFERENCES `kunden` (`knr`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `rechnungspositionen`
--
ALTER TABLE `rechnungspositionen`
  ADD CONSTRAINT `fk_rechnungspositionen_artikel` FOREIGN KEY (`artnr`) REFERENCES `artikel` (`artnr`) ON DELETE RESTRICT ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_rechnungspositionen_rechnungen` FOREIGN KEY (`rnr`) REFERENCES `rechnungen` (`rnr`) ON DELETE RESTRICT ON UPDATE RESTRICT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
