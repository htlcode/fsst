<table>
<tr><th>Nr</th><th>Titel</th><th>ECTS</th><th>Prof</th></tr>
<?php
    $user = 'root';
    $pass = '';
    session_start();
    $conn = new PDO("mysql:host=localhost;uni", $user, $pass);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $conn->setAttribute(PDO::ATTR_CASE, PDO::CASE_UPPER);
    $conn->beginTransaction();
    
    # anfrage senden
    $sql = "SELECT lect.ID AS LECTID, lect.NAME AS LECTNAME, ECTS, prof.NAME AS PROFNAME FROM uni.lect, uni.prof WHERE lect.profid=prof.id";
    $stmt = $conn->query($sql);

    while ($row = $stmt->fetch()):
        echo "<tr>";
        echo "<td>".$row['LECTID']."</td>";
        echo "<td>".$row['LECTNAME']."</td>";
        echo "<td>".$row['ECTS']."</td>";
        echo "<td>".$row['PROFNAME']."</td>";
        echo "</tr>\n";
    endwhile;
    $_SESSION['user'] = $_POST['user'];
    # transaktion beenden
    $conn->commit();
    $conn = null;
?>
</table>