<table>
<tr><th>Nr</th><th>Name</th></tr>
<?php
    $user = 'root';
    $pass = '';

    session_start();
    $conn = new PDO("mysql:host=localhost;uni", $user, $pass);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $conn->setAttribute(PDO::ATTR_CASE, PDO::CASE_UPPER);
    $conn->beginTransaction();
    
    # anfrage senden
    $sql = "SELECT ID, NAME FROM uni.prof UNION SELECT ID, NAME FROM uni.ass";
    $stmt = $conn->query($sql);

    while ($row = $stmt->fetch()):
        echo "<tr>";
        echo "<td>".$row['ID']."</td>";
        echo "<td>".$row['NAME']."</td>";
        echo "</tr>\n";
    endwhile;

    $_SESSION['user'] = $_POST['user'];

    # transaktion beenden
    $conn->commit();
    $conn = null;
?>
</table>