-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Erstellungszeit: 17. Okt 2022 um 15:09
-- Server-Version: 10.4.24-MariaDB
-- PHP-Version: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Datenbank: `uni`
--

-- --------------------------------------------------------

DROP SCHEMA IF EXISTS uni;
CREATE SCHEMA uni;

USE uni;
--
-- Tabellenstruktur für Tabelle `ass`
--

CREATE TABLE `ass` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `expertise` varchar(45) DEFAULT NULL,
  `profid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `cond`
--

CREATE TABLE `cond` (
  `lectcond` int(11) NOT NULL,
  `lectid` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `exam`
--

CREATE TABLE `exam` (
  `studid` int(11) NOT NULL,
  `profid` int(11) NOT NULL,
  `lectid` int(11) NOT NULL,
  `date` date NOT NULL,
  `grade` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `lect`
--

CREATE TABLE `lect` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `ects` int(11) DEFAULT NULL,
  `profid` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `prof`
--

CREATE TABLE `prof` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `room` varchar(45) DEFAULT NULL,
  `degree` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `stud`
--

CREATE TABLE `stud` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `sem` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `visit`
--

CREATE TABLE `visit` (
  `lectid` int(11) NOT NULL,
  `studid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `ass`
--
ALTER TABLE `ass`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_ass_prof` (`profid`);

--
-- Indizes für die Tabelle `cond`
--
ALTER TABLE `cond`
  ADD PRIMARY KEY (`lectcond`),
  ADD KEY `fk_cond_lect` (`lectid`);

--
-- Indizes für die Tabelle `exam`
--
ALTER TABLE `exam`
  ADD PRIMARY KEY (`studid`,`lectid`,`date`,`profid`),
  ADD KEY `fk_exam_prof` (`profid`),
  ADD KEY `fk_exam_lect` (`lectid`);

--
-- Indizes für die Tabelle `lect`
--
ALTER TABLE `lect`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_lect_prof` (`profid`);

--
-- Indizes für die Tabelle `prof`
--
ALTER TABLE `prof`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `stud`
--
ALTER TABLE `stud`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `visit`
--
ALTER TABLE `visit`
  ADD PRIMARY KEY (`lectid`,`studid`),
  ADD KEY `fk_visit_stud` (`studid`);

--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `ass`
--
ALTER TABLE `ass`
  ADD CONSTRAINT `fk_ass_prof` FOREIGN KEY (`profid`) REFERENCES `prof` (`id`) ON UPDATE CASCADE;

--
-- Constraints der Tabelle `cond`
--
ALTER TABLE `cond`
  ADD CONSTRAINT `fk_cond_lect` FOREIGN KEY (`lectid`) REFERENCES `lect` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_lect_cond` FOREIGN KEY (`lectcond`) REFERENCES `lect` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints der Tabelle `exam`
--
ALTER TABLE `exam`
  ADD CONSTRAINT `fk_exam_lect` FOREIGN KEY (`lectid`) REFERENCES `lect` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_exam_prof` FOREIGN KEY (`profid`) REFERENCES `prof` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_exam_stud` FOREIGN KEY (`studid`) REFERENCES `stud` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints der Tabelle `lect`
--
ALTER TABLE `lect`
  ADD CONSTRAINT `fk_lect_prof` FOREIGN KEY (`profid`) REFERENCES `prof` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints der Tabelle `visit`
--
ALTER TABLE `visit`
  ADD CONSTRAINT `fk_visit_lect` FOREIGN KEY (`lectid`) REFERENCES `lect` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_visit_stud` FOREIGN KEY (`studid`) REFERENCES `stud` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;