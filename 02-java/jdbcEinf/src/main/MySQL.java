package main;

import java.sql.*;

public class MySQL {
	
    public static void main(String[] args) throws Exception {
        String sDbUrl="jdbc:mysql://localhost:3306/uni";
        String sUsr="root";
        String sPwd="";
        String sTable="prof";

        Connection conn;
        Statement stmt;
        ResultSet rslt;

        conn = DriverManager.getConnection( sDbUrl, sUsr, sPwd );
        stmt = conn.createStatement();
        rslt = stmt.executeQuery("SELECT * FROM " + sTable);

        while( rslt.next() ) {
            System.out.printf("%-6s %-15s %-4s %-4s\n", rslt.getString(1), rslt.getString(2), rslt.getString(3), rslt.getString(4));
        }
        conn.close();
    }
}
