str = '1234';

% der ascii wert jedes zeichens wird gespeichert
num1 = double(str);

% um die zahl in eine numerische zahl umzuwandeln
num2 = str2double(str);

% oder
num3 = str2num(str);