figure(1)
hold on
t = linspace(0, 10, 1001);
x = ones(1, length(t));
y = 5*sin(2*t + pi/8).*exp(-.5*t)+2;      % .*      für jedes element
plot(t, x, 'b', 'LineWidth', 2);
plot(t, y, 'r', 'LineWidth', 2);
axis([0 10 -4 8])
set(gca, 'YTick', [-4:2:8])
grid on
title('Gedämpfte Schwingung', 'FontSize', 14, 'FontWeight', 'bold')
xlabel('s/t')
ylabel('Functionswert')
text(0.5, 6.4, 'Maximum von y', 'FontName', 'Vardana', 'FontAngle', 'Italic', 'BackgroundColor', [0.8, 0.8, 0.8])
h_legend = legend('x_{(t)}', 'y_{(t)}')
set(h_legend, 'FontSize', 14)
