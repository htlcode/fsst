t = linspace(0, 10, 1001);
x = ones(1, length(t));
y = 5*sin(2*t+pi/8).*exp(-.5*t)+2;
fprintf('t = %4.2f x = %1.0f y = %6.4f\n',[t;x;y]);

fid = fopen('sinus.txt', 'w');
fprintf(fid, 't = %4.2f x = %1.0f y = %6.4f\n',[t;x;y]);
fclose(fid);