pkg load control

Zaehler = [3 2 1]
Nenner = [10 0 1]
F = tf(Zaehler, Nenner)
roots(Nenner)
figure(1)

pzmap(F)

figure(3)
step(F)
% ax = findall(h1, 'type', 'axes');


figure(2)
bode(F)

h1 = gcf
h2 = gca

ax = findall (h1, 'type', 'axes');
pl = findall (h1, 'type', 'line');

set(pl(6), 'linewidth', 2, 'color', 'blue');
set(pl(5), 'linewidth', 2, 'color', 'blue');
