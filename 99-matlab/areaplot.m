[X,Y] = meshgrid([-10:0.5:10],[-10:0.5:10]);
Z = X.^2 -Y.^2;
surf(X,Y,Z)
grid on
title('surf()','FontSize',20)
xlabel('x','FontSize',20)
ylabel('y','FontSize',20)
