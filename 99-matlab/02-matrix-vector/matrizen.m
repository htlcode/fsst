% 1x6 matrix - zeilenvektor

z = [1, 2, 3, 4, 5, 6];

% 6x1 matrix - spaltenvektor

s = [1; 2; 3; 4; 5; 6];

% summe der einzelnen elemente
sum1 = sum(z);
sum2 = sum(s);

% produkt der einzelnen elemente
prod1 = prod(z);
prod2 = prod(s);

% zeilenvektoren können mit einem hochkomma in spaltenvektoren umgewandelt
% werden
z3 = z';

% die dimension einer Matrix kann mit size() abgefragt werden
% wenn mehrere werte zurückgegeben werden, müssen diese durch kommas 
% getrennt inin [] angegeben werden

[m, n] = size(z);

A = [1, 2, 3, 3;
     4, 5, 6, 3;
     7, 8, 9, 3;];

A2 = [1, 2;
     3, 4;
     5, 6];

len1 = length(z); % bei zeilenvektor (1xn) werden Spalten n zurückgegeben  
len2 = length(s); % bei spaltenvektor (mx1) werden Zeilen m zurückgegeben
lenA = length(A); % bei matrix gibt es die länge der längsten dimension zurück
lenA2 = length(A2);

% mit A' erhält man die transponierte
At = A';

% sum berechnet die summe einer jeden spalte
sumA2 = sum(A2);

