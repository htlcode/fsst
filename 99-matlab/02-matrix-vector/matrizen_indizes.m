% zeilenvektor
z = [1 2 3 4 5 6];

% spaltenvektor
s = [1; 2; 3; 4; 5; 6];

e21 = z(2);
e12 = s(2);

A = [1 2 3;
     4 5 6;
     7 8 9];

z2 = A(2, :);
s2 = A(:, 2);

B = A(:, :); % identisch mit a
C = A(1:2, 1:2); % untermatrix von a

