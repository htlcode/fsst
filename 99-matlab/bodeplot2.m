pkg load control

s = tf('s')

T1 = 0.01
T2 = 0.1
T3 = 1
T4 = 10
K1 = 6

H1 = K1 * ((1+s*T1))/((1+s*T2)*(1+2*T3)*(1+s*T4));
figure(1)
bode(H1)

figure(2)
nyquist(H1)
