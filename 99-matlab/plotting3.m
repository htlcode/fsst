t = 0:0.01:100;
x = (1+t.^2).*sin(t);
y = (1+t.^2).*cos(t);
plot3(x,y,t,'r','LineWidth',2)
grid on
title('plot3()','FontSize',20)
xlabel('x (t)','FontSize',20)
ylabel('y (t)','FontSize',20)
